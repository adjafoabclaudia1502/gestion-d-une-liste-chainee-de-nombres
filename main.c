#include "nombres.h"
#include <stdio.h>
#include <locale.h>

int main() {
    setlocale(LC_ALL, "");

    Maillon* liste = NULL;
    int choix = 0;
    int nombre, position;
    printf("\nPROGRAMME:LA GESTION DE LISTE CHAINEE DE NOMBRES");
    printf("\v");
    printf("\nIl s'agit d'une liste chain�e de nombres dont � chaque fois vous pouvez ajouter un ou plusieurs nombres ou les supprimer");
    printf("\n");

    while (choix != 9) {
        printf("Menu:\n");
        printf("1. Afficher la liste\n");
        printf("2. Ajouter un nombre en t�te de liste\n");
        printf("3. Ajouter un nombre en fin de liste\n");
        printf("4. Ajouter un nombre � une position donn�e\n");
        printf("5. Supprimer un nombre en t�te de liste\n");
        printf("6. Supprimer un nombre en fin de liste\n");
        printf("7. Supprimer un nombre � une position donn�e\n");
        printf("8. Calculer la longueur de la liste\n");
        printf("9. Quitter\n");
        printf("Votre choix : ");
        scanf("%d", &choix);

        switch (choix) {
            case 1:
                printf("Liste : ");
                afficherListe(liste);
                break;
            case 2:
                printf("Entrez le nombre � ajouter en t�te : ");
                scanf("%d", &nombre);
                ajouterEnTete(&liste, nombre);
                break;
            case 3:
                printf("Entrez le nombre � ajouter en fin : ");
                scanf("%d", &nombre);
                ajouterEnFin(&liste, nombre);
                break;
            case 4:
                printf("Entrez le nombre � ajouter : ");
                scanf("%d", &nombre);
                printf("Entrez la position : ");
                scanf("%d", &position);
                ajouterAPosition(&liste, nombre, position);
                break;
            case 5:
                supprimerEnTete(&liste);
                break;
            case 6:
                supprimerEnFin(&liste);
                break;
            case 7:
                printf("Entrez la position du nombre � supprimer : ");
                scanf("%d", &position);
                supprimerAPosition(&liste, position);
                break;
            case 8:
                printf("Longueur de la liste : %d\n", longueurListe(liste));
                break;
            case 9:
                printf("Programme termin�.\n");
                break;
            default:
                printf("Choix invalide. Veuillez r�essayer.\n");
                break;
        }
    }

    return 0;
}
